#!/bin/bash

DEVICE="$1"
BRANCH="$2"
# setup env

case $BRANCH in
	"bb")
	SVNPATH="branches/barrier_breaker"
	;;
	"cc")
	SVNPATH="branches/chaos_calmer"
	;;
	"trunk")
	SVNPATH="trunk"
	;;
	*)
	echo "Specify a valid branch as the second argument (bb or trunk)"
	exit 1
	;;
esac

if [ -z $DEVICE ]; then
	echo "No device specified. Specify device as first argument" 
	echo "Exiting"
	exit 1
fi
if [ -f ./$DEVICE/$BRANCH/$DEVICE.config.init ] ; then
	echo "Building $2 for $DEVICE"
else
	echo "$DEVICE not found. Exiting"
	exit 1
fi


echo "Cleaning old dirs in 5s"
sleep 5
WORKDIR=$PWD/$DEVICE-$BRANCH-workdir
DLDIR=$PWD/$DEVICE-$BRANCH-dl
BINDIR=$PWD/binaries
rm -rfv $WORKDIR $DLDIR $BINDIR
mkdir $WORKDIR $DLDIR $BINDIR
ln -s $DLDIR $WORKDIR/dl
cd $WORKDIR

echo "Checking out trunk"
svn co svn://svn.openwrt.org/openwrt/$SVNPATH .
echo "Updating feeds"
./scripts/feeds update -a

echo "Global patches"
for i in `ls $WORKDIR/../global/$BRANCH/*.patch`; do
	echo $i
	patch -p1 -i $i
done
echo "Applied global patches"

echo "Device specific patches"
for i in `ls $WORKDIR/../$DEVICE/$BRANCH/*.patch`; do
	patch -p1 -i $i
done
echo "Applied device patches"
sleep 5

echo "Installing packages"
./scripts/feeds install -a
cp $WORKDIR/../$DEVICE/$BRANCH/$DEVICE.config.init .config.init

echo "Commiting changes"

svn add .config.init
svn status | cut -d ? -f 2 -s | xargs -r svn add
(cd $WORKDIR/feeds/luci; git add -A)
(cd $WORKDIR/feeds/packages; git add -A)

cp .config.init .config

echo "Buildenv done"

echo "Starting build in 5s"
sleep 5
make defconfig
make download
make -j 5 | tee build.log
echo "Build finished"
